import { PluginOptions } from "./PluginOptions";

export function resolveOptions(
    options: Partial<PluginOptions>,
    config: any
): PluginOptions {
    const resolved: PluginOptions = {
        pushUrl: config.env.NUGET_PUSH_URL ?? "",
        apiKey: config.env.NUGET_TOKEN,
        packArguments: [],
        pushFiles: ["*.nupkg"],
        debug: false,
        ...options,
    };

    return resolved;
}
