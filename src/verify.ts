import { resolveOptions } from "./options";
import { PluginOptions } from "./PluginOptions";
import * as child from "child_process";

export async function verifyConditions(
    options: PluginOptions,
    config: any
): Promise<void> {
    // Resolve the options and make sure we have them all populated.
    const resolved = resolveOptions(options, config);

    // Set up debugging.
    const { logger } = config;
    const debug = resolved.debug ? logger.debug : () => {};
    logger.debug = debug;

    debug("options", resolved);

    // Verify the conditions.
    let errors: any[] = [];

    if (!resolved.pushUrl || resolved.pushUrl === "") {
        errors.push("NUGET_PUSH_URL or the pushUrl options must be set");
    }

    if (!resolved.apiKey || resolved.apiKey === "") {
        errors.push("NUGET_TOKEN or apiKey option must be set");
    }

    // Make sure we have access to the `dotnet nuget` command.
    try {
        const output = child.spawnSync("dotnet", ["nuget", "--version"]);

        if (output.status !== 0) {
            errors.push(
                "The `dotnet nuget --version` responded with an error code"
            );
        } else {
            const lines = output.stdout.toString().trim().split(/\r?\n/);

            if (lines[0] !== "NuGet Command Line") {
                errors.push(
                    "`dotnet nuget --version` didn't respond as expected: " +
                        lines.join(", ")
                );
            }

            logger.info("Using NuGet version " + lines[1]);
        }
    } catch {
        errors.push("Cannot run the `dotnet` process");
    }

    // If we have any errors, throw a combined error.
    if (errors.length > 0) {
        for (const error of errors) {
            logger.error(error);
        }

        throw new Error("Could not verify conditions for NuGet");
    }
}
